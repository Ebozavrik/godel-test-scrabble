<?php

namespace AppBundle\Controller;

use AppBundle\Entity\GameScore;
use AppBundle\Entity\Player;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $topPlayers = $this->getDoctrine()->getRepository(GameScore::class)->getTopPlayers();

        return $this->render('default/index.html.twig', array('players' => $topPlayers));
    }
}
